package com.semanticsquare.thillio.entities;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.semanticsquare.thillio.constants.MovieGenre;
import com.semanticsquare.thillio.managers.BookmarkManager;

class MovieTest {

	@Test
	void testIsKidfriendlyEligible() {
		//Test1: Genre is thriller
		Movie movie=BookmarkManager.getInstance().createMovie(3000,"Citizen Kane","",1941,new String[] {"Orson Welles","Joseph Cotten"},new String[]{"Orson Welles"},MovieGenre.THRILLERS,8.5);
		boolean iskidFriendlyEligible=movie.isKidfriendlyEligible();
		assertFalse("Thriller-fail",iskidFriendlyEligible);
		//Test2: Genre is horror
	}

}
