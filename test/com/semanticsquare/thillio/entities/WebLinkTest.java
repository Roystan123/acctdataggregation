package com.semanticsquare.thillio.entities;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.semanticsquare.thillio.managers.BookmarkManager;

class WebLinkTest {

	@Test
	void testIsKidfriendlyEligible() {
		//Test1: porn in url-false
		Weblink weblink=BookmarkManager.getInstance().createWeblink(2000,"Taming Tiger, Part 2","http://www.javaworld.com/article/2072759/core-java/taming-porn--part-2.html","http://www.javaworld.com");
		boolean iskidfriendlyEligible=weblink.isKidfriendlyEligible();
		assertFalse("porn in url-fails",iskidfriendlyEligible);
		
		//Test2: porn in title-false 
		Weblink weblink1=BookmarkManager.getInstance().createWeblink(2000,"Taming PORN, Part 2","http://www.javaworld.com/article/2072759/core-java/taming-tiger--part-2.html","http://www.javaworld.com");
		boolean iskidfriendlyEligible1=weblink1.isKidfriendlyEligible();
		assertFalse("porn in title-fails",iskidfriendlyEligible1);
		
	    //Test3: adult in host-false
		Weblink weblink2=BookmarkManager.getInstance().createWeblink(2000,"Taming Tiger, Part 2","http://www.javaworld.com/article/2072759/core-java/taming-tiger--part-2.html","http://www.adult.com");
		boolean iskidfriendlyEligible2=weblink2.isKidfriendlyEligible();
		assertFalse("adult in host-fails",iskidfriendlyEligible2);
		
		//Test4: adult in url, but not in host-true
		Weblink weblink3=BookmarkManager.getInstance().createWeblink(2000,"Taming Tiger, Part 2","http://www.javaworld.com/article/2072759/core-java/taming-tiger--adult-2.html","http://www.javaworld.com");
		boolean iskidfriendlyEligible3=weblink3.isKidfriendlyEligible();
		assertTrue("adult in url,but not in host-fails",iskidfriendlyEligible3);
		
		//Test5: adult in title only-true
		Weblink weblink4=BookmarkManager.getInstance().createWeblink(2000,"Taming adult, Part 2","http://www.javaworld.com/article/2072759/core-java/taming-tiger--part-2.html","http://www.javaworld.com");
		boolean iskidfriendlyEligible4=weblink4.isKidfriendlyEligible();
		assertTrue("adult in title-fails",iskidfriendlyEligible4);
	}

}
