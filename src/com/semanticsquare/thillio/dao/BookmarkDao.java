package com.semanticsquare.thillio.dao;

import com.semanticsquare.thillio.datastore.DataStore;
import com.semanticsquare.thillio.entities.Bookmark;
import com.semanticsquare.thillio.entities.UserBookmark;

public class BookmarkDao {
public Bookmark[][] getBookmark(){
	return DataStore.getBookmarks();
}

public void saveUserBookmark(UserBookmark userBookmark) {
	DataStore.add(userBookmark);
	
}

}
