package com.semanticsquare.thillio.controllers;

import com.semanticsquare.thillio.entities.Bookmark;
import com.semanticsquare.thillio.entities.User;
import com.semanticsquare.thillio.entities.UserBookmark;
import com.semanticsquare.thillio.managers.BookmarkManager;

public class BookmarkController {
	private static BookmarkController bookmarkController = new BookmarkController();
	
	
	private BookmarkController() {
	}

	public static BookmarkController getInstance() {
		return bookmarkController;
	}

	public void saveUserBookmark(User user, Bookmark bookmark) {
		BookmarkManager.getInstance().saveUserBookmark(user,bookmark);
	}

	public void setKidFriendlyStatus(User user, String Status, Bookmark bookmark) {
		BookmarkManager.getInstance().setKidFriendlyStatus(user,Status,bookmark);
		
	}

	public void share(User user, Bookmark bookmark) {
		BookmarkManager.getInstance().share(user,bookmark);
		
	}
}
