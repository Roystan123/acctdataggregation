package com.semanticsquare.thillio.constants;

public class KidFriendlyStatus {
private KidFriendlyStatus() {}
public static final String APPROVED="approved";
public static final String REJECTED="rejected";
public static final String UNKNOWN="Unknown";
}
