package com.semanticsquare.thillio.entities;

import java.util.Arrays;

import com.semanticsquare.thillio.partner.Shareable;

public class Weblink extends Bookmark implements Shareable {
 private String url;
 private String host;
public String getUrl() {
	return url;
}
public void setUrl(String url) {
	this.url = url;
}
public String getHost() {
	return host;
}
public void setHost(String host) {
	this.host = host;
}
/* (non-Javadoc)
 * @see java.lang.Object#toString()
 */
@Override
public String toString() {
	return "Weblink [id="+ getId() +"  title="+ getTitle()+"  url=" + url + ", host=" + host + "]";
}
@Override
public boolean isKidfriendlyEligible() {
  
if(url.toLowerCase().contains("porn")||getTitle().toLowerCase().contains("porn")||host.toLowerCase().contains("adult"))	
{
	return false;
}
else 
{
	return true;
}
}
public String getItemData() {
	StringBuilder builder=new StringBuilder();
	builder.append("<Item>");
	builder.append("<type>Weblink</type>");
	builder.append("<title>").append(getTitle()).append("</title>");
	builder.append("<url>").append(url).append("<url>");
	builder.append("<host>").append(host).append("</host>");
    builder.append("</Item>");
	return builder.toString();
}

}



