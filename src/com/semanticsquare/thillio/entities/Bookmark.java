package com.semanticsquare.thillio.entities;

import com.semanticsquare.thillio.constants.KidFriendlyStatus;

public abstract class Bookmark {
 private long id;
 private String title;
 private String profileUrl;
 private String kidFriendlyStatus=KidFriendlyStatus.UNKNOWN;
 private User kidFriendlymarkedby;
 private User sharedBy;
public User getSharedBy() {
	return sharedBy;
}
public void setSharedBy(User sharedBy) {
	this.sharedBy = sharedBy;
}
public String getKidFriendlyStatus() {
	return kidFriendlyStatus;
}
public void setKidFriendlyStatus(String kidFriendlyStatus) {
	this.kidFriendlyStatus = kidFriendlyStatus;
}
public long getId() {
	return id;
}
public void setId(long id) {
	this.id = id;
}
public User getKidFriendlymarkedby() {
	return kidFriendlymarkedby;
}
public void setKidFriendlymarkedby(User kidFriendlymarkedby) {
	this.kidFriendlymarkedby = kidFriendlymarkedby;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getProfileUrl() {
	return profileUrl;
}
public void setProfileUrl(String profileUrl) {
	this.profileUrl = profileUrl;
}
public abstract boolean isKidfriendlyEligible();

}
