package com.semanticsquare.thillio.entities;

import java.util.Arrays;
import com.semanticsquare.thillio.partner.Shareable;

public class Book extends Bookmark implements Shareable {
 private int publicationYear;
 private String publisher;
 private String[] authors;
 private String genre;
 private double amazonRating;
public int getPublicationYear() {
	return publicationYear;
}
public void setPublicationYear(int publicationYear) {
	this.publicationYear = publicationYear;
}
public String getPublisher() {
	return publisher;
}
public void setPublisher(String publisher) {
	this.publisher = publisher;
}
public String[] getAuthors() {
	return authors;
}
public void setAuthors(String[] authors) {
	this.authors = authors;
}
public String getGenre() {
	return genre;
}
public void setGenre(String genre) {
	this.genre = genre;
}
public double getAmazonRating() {
	return amazonRating;
}
public void setAmazonRating(double amazonRating) {
	this.amazonRating = amazonRating;
}
/* (non-Javadoc)
 * @see java.lang.Object#toString()
 */
@Override
public String toString() {
	return "Book [id="+ getId() +"  title="+ getTitle()+ "  publicationYear=" + publicationYear + ", publisher=" + publisher + ", authors="
			+ Arrays.toString(authors) + ", genre=" + genre + ", amazonRating=" + amazonRating + "]";
}
@Override
public boolean isKidfriendlyEligible() {
if(genre.toLowerCase().contains("philosophy")||genre.toLowerCase().contains("self help")||genre.toLowerCase().contains("romance"))
{
	return false;
}
else {
return true;	
	
}
}
@Override
public String getItemData() {
	StringBuilder builder=new StringBuilder();
	builder.append("<Item>");
	builder.append("<type>Book</type>");
	builder.append("<title>").append(getTitle()).append("</title>");
	builder.append("<authors>").append(Arrays.toString(authors).substring(1,(Arrays.toString(authors).length()-1))).append("</authors>");
	builder.append("<publisher>").append(publisher).append("</publisher>");
	builder.append("<publicationYear>").append(publicationYear).append("</publicationYear>");
	builder.append("<genre>").append(genre).append("</genre>");
	builder.append("<amazonRating>").append(amazonRating).append("</amazonRating>");
	builder.append("</Item>");
	return builder.toString();
}
}
