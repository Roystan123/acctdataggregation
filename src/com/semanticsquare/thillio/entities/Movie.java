package com.semanticsquare.thillio.entities;

import java.util.Arrays;

public class Movie extends Bookmark {
	private int releaseYear;
	private String[] cast;
	private String[] directors;
	private String genre;
	private double imdbRatings;

	public int getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(int releaseYear) {
		this.releaseYear = releaseYear;
	}

	public String[] getCast() {
		return cast;
	}

	public void setCast(String[] cast) {
		this.cast = cast;
	}

	public String[] getDirectors() {
		return directors;
	}

	public void setDirectors(String[] directors) {
		this.directors = directors;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public double getImdbRatings() {
		return imdbRatings;
	}

	public void setImdbRatings(double imdbRatings) {
		this.imdbRatings = imdbRatings;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Movie [id=" + getId() + "  title=" + getTitle() + "profileUrl" + getProfileUrl() + "nreleaseYear="
				+ releaseYear + ", cast=" + Arrays.toString(cast) + ", directors=" + Arrays.toString(directors)
				+ ", genre=" + genre + ", imdbRatings=" + imdbRatings + "]";
	}

	@Override
	public boolean isKidfriendlyEligible() {
		if (genre.toLowerCase().contains("thrillers") || genre.toLowerCase().contains("foreign thrillers")
				|| genre.toLowerCase().contains("gay & lesbian") || genre.toLowerCase().contains("romantic movies")) {
			return false;
		} else {
			return true;
		}
	}
}
