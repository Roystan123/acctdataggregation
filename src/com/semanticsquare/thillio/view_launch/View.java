package com.semanticsquare.thillio.view_launch;

import com.semanticsquare.thillio.constants.KidFriendlyStatus;
import com.semanticsquare.thillio.constants.UserType;
import com.semanticsquare.thillio.controllers.BookmarkController;
import com.semanticsquare.thillio.datastore.DataStore;
import com.semanticsquare.thillio.entities.Bookmark;
import com.semanticsquare.thillio.entities.User;
import com.semanticsquare.thillio.partner.Shareable;

public class View {
    public static void bookmark(User user,Bookmark[][] bookmarks) {
    	System.out.println("\n"+user.getEmail()+"is bookmarking");
    	for(int i=0;i<DataStore.USER_BOOKMARK_LIMIT;i++) {
    		int typeoffset=0; //(int)(Math.random()*DataStore.BOOKMARK_USER_TYPE_COUNT);
    		int bookmarkoffset= (int)(Math.random()*DataStore.BOOMARK_PER_TYPE_COUNT);
    		Bookmark bookmark=bookmarks[typeoffset][bookmarkoffset];
    		BookmarkController.getInstance().saveUserBookmark(user,bookmark);
    		System.out.println(bookmark);
    	}
    	
    	
    }
    public static void browse(User user,Bookmark[][] bookmarks) {
    	System.out.println("\n"+user.getEmail()+"is browsing");
    	int bookmarkcount=0;
    	int bookmarkcountt=0;
    	for(Bookmark[] bookmarklist:bookmarks) {
    	for(Bookmark bookmark:bookmarklist) {
    		if(!(user.getUserType().equals(UserType.EDITOR)||user.getUserType().equals(UserType.CHEIF_EDITOR)))
    	{		
    	if(bookmarkcount<DataStore.USER_BOOKMARK_LIMIT) {
    	boolean isBookmarked=getBookmarkDecision(bookmark);	
    	if(isBookmarked) {
    		bookmarkcount++;
    		BookmarkController.getInstance().saveUserBookmark(user,bookmark);
    		System.out.println("New item bookmarked:"+bookmark);
    	}
    	}
    	}
    else 
    {	
   	if(bookmarkcountt<DataStore.USER_BOOKMARK_LIMIT) {
    	if(user.getUserType().equals(UserType.EDITOR)||user.getUserType().equals(UserType.CHEIF_EDITOR)) {
    		if(bookmark.isKidfriendlyEligible()&&bookmark.getKidFriendlyStatus().equals(KidFriendlyStatus.UNKNOWN)) {
    			String Status=getkidfriendlystatusupdate(bookmark);
    			if(!(Status.equals(KidFriendlyStatus.UNKNOWN))) {
    				BookmarkController.getInstance().setKidFriendlyStatus(user,Status,bookmark);
    				
  				bookmarkcountt++;
    			}
    			if((bookmark.getKidFriendlyStatus().equals(KidFriendlyStatus.APPROVED))&&(bookmark instanceof Shareable)){
    				boolean isShared=getShareDecision();
    				if(isShared) {
    					BookmarkController.getInstance().share(user,bookmark);
    				}
    			}
    			
    		}
    	}
    }
  }	
 }	
 }
    }
    	
 

	private static boolean getShareDecision() {
		return Math.random()<0.5?true:false;
		
	}
	private static String getkidfriendlystatusupdate(Bookmark bookmark) {
		return Math.random()<0.4?KidFriendlyStatus.APPROVED:(Math.random()>=0.4&&Math.random()<0.8)?KidFriendlyStatus.REJECTED:KidFriendlyStatus.UNKNOWN;
		
	}
	private static boolean getBookmarkDecision(Bookmark bookmark) {
		return Math.random()<0.5?true:false;
	}

    }