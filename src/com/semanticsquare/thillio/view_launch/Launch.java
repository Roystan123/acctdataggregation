package com.semanticsquare.thillio.view_launch;

import com.semanticsquare.thillio.datastore.DataStore;
import com.semanticsquare.thillio.entities.Bookmark;
import com.semanticsquare.thillio.entities.User;
import com.semanticsquare.thillio.managers.BookmarkManager;
import com.semanticsquare.thillio.managers.UserManager;

public class Launch {
	private static User[] users;
	private static Bookmark[][] bookmarks;
	private static void loadData() {
		System.out.print("1. Loading_data!!!");
		DataStore.loadData();
		users=UserManager.getInstance().getUser();
		bookmarks=BookmarkManager.getInstance().getBookmark();
		System.out.print("Loading_data!!!");
		printUserdata();
		printBookmars();
		
	}

	

	private static void printBookmars() {
		for(Bookmark[] i:bookmarks) {
			for(Bookmark arr:i) {
				System.out.println(arr);
			}
		}
	}

	private static void printUserdata() {
		for(User user:users) {
			System.out.println(user);
		}
	}
	private static void startBookmarking() {
		System.out.println("2. Start Bookmarking");
		for(User user:users) {
			View.bookmark(user, bookmarks);
		}
	}
	private static void startBrowsing() {
		
		System.out.println("3. Start Browsing");
		for(User user:users) {
			View.browse(user, bookmarks);
		}
	}


	public static void main(String[] args) {
		loadData();
        startBookmarking();
        startBrowsing();
	}

	


}
