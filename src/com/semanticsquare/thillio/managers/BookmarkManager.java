package com.semanticsquare.thillio.managers;

import com.semanticsquare.thillio.dao.BookmarkDao;
import com.semanticsquare.thillio.dao.UserDao;
import com.semanticsquare.thillio.entities.Book;
import com.semanticsquare.thillio.entities.Bookmark;
import com.semanticsquare.thillio.entities.Movie;
import com.semanticsquare.thillio.entities.User;
import com.semanticsquare.thillio.entities.UserBookmark;
import com.semanticsquare.thillio.entities.Weblink;

public class BookmarkManager {
	private static BookmarkManager bookmarkManager = new BookmarkManager();
	private static BookmarkDao dao=new BookmarkDao();
	private BookmarkManager() {
	}

	public static BookmarkManager getInstance() {
		return bookmarkManager;
	}

	public Movie createMovie(long id, String title, String profileUrl, int releaseYear, String[] cast,
			String[] directors, String genre, double imdbRatings) {
		Movie movie = new Movie();
		movie.setId(id);
		movie.setTitle(title);
		movie.setProfileUrl(profileUrl);
		movie.setReleaseYear(releaseYear);
		movie.setCast(cast);
		movie.setDirectors(directors);
		movie.setGenre(genre);
		movie.setImdbRatings(imdbRatings);
		return movie;
	}

	public Book createBook(long id, String title, int publicationYear, String publisher,
			String[] authors, String genre, double amazonRating) {
		Book book=new Book();
		book.setId(id);
		book.setTitle(title);
		book.setPublisher(publisher);
		book.setPublicationYear(publicationYear);
		book.setAuthors(authors);
		book.setGenre(genre);
		book.setAmazonRating(amazonRating);
		
		return book;
		
	}

    public Weblink createWeblink(long id, String title,String url,String host) {
    	Weblink weblink=new Weblink();
    	weblink.setHost(host);
    	weblink.setId(id);
    	weblink.setTitle(title);
    	weblink.setUrl(url);
    	return weblink;
    	
    }
    public Bookmark[][] getBookmark() {
    	return dao.getBookmark(); 
    	
    }

	public void saveUserBookmark(User user, Bookmark bookmark) {
		UserBookmark userBookmark=new UserBookmark();
		userBookmark.setBookmark(bookmark);
		userBookmark.setUser(user);
		dao.saveUserBookmark(userBookmark);
		
	}

	public void setKidFriendlyStatus(User user, String Status, Bookmark bookmark) {
		bookmark.setKidFriendlyStatus(Status);
		bookmark.setKidFriendlymarkedby(user);
		System.out.println("Kid friendly status:"+Status+", marked by "+user.getEmail()+", "+bookmark);
		
	}

	public void share(User user, Bookmark bookmark) {
		bookmark.setSharedBy(user);
		System.out.println("data shared by");
		if(bookmark instanceof Book) {
			System.out.println(((Book)bookmark).getItemData());
		}
		else if(bookmark instanceof Weblink){
			System.out.println(((Weblink)bookmark).getItemData());
		}
	}
}
