package com.semanticsquare.thillio.managers;

import com.semanticsquare.thillio.dao.UserDao;
import com.semanticsquare.thillio.entities.User;

public class UserManager {
	private static UserManager userManager = new UserManager();
	private static UserDao dao=new UserDao();
	private UserManager() {
	}

	public static UserManager getInstance() {
		return userManager;
	}

	public User createUser(long id, String email,String password, String firstName, String lastName, int gender,
			String userType) {
		User user=new User();
		user.setId(id);
		user.setPassword(password);
		user.setGender(gender);
		user.setFirstName(firstName);
		user.setEmail(email);
		user.setLastName(lastName);
		user.setUserType(userType);

		return user;
	}
public User[] getUser() {
	return dao.getUsers();
	
}
}
